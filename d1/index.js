/*
	MongoDB - Query operator
	Expand query in mongoDB using query operator

*/ 

/*
	Query operator
	
	Types of query operator

	1. comparison query operator
	-greater than
	-greater than or equal
	-less than
	-less than or equal
	-not equal to
	-in

	2. Logical Operator
	-OR
	-AND

	field projection
	1. Inclusion
	2. Exclusion
*/

/*
	what does query operator mean?
	-A "Query" is a request for data from a database.
	-An "Operator" is a symbol that represents an action or a process

	by using Query we can do more than what we do in CRUD operation.

*/

// 1. COMAPIRON QUERY OPERATOS
/*
	INCLUDES:
	-greater than
	-greater than or equal
	-less than
	-less than or equal
	-not equal to
	-in
*/

/*
	1.1 Greater than: "$gt"
	syntax:
		db.collectionName.find({field: {$gt: value}})
*/
db.users.find({age: {$gt: 76}});

/*
	1.2 Greater than or equal to: $gte
	syntax:
		db.colectionName.find({field: {$gte: value}})
*/
db.users.find({age: {$gte: 76}});

/*
	1.3 less than operator : $lt
	syntax:
		db.colectionName.find({field: {$lt: value}})
*/
db.users.find({age: {$lt: 65}});

/*
	1.4 less than or equal to : $lte
	syntax
		db.colectionName.find({field: {$lte: value}})
*/
db.users.find({age: {$lte: 65}});


/*
	1.5 not equal to : $ne
	syntax
		db.colectionName.find({field: {$ne: value}})
*/

db.users.find({age: {$ne: 65}});

/*
	1.5 IN : $in
	- finds docunments wuth specific much createria on one field using different values
	syntax
		db.colectionName.find({field: {$in: value}})
*/
db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({courses: {$in: ["HTML", "React"]}});


// 2. EVALUATION OPERATORS
// either individual fields or the entire 
//collection's document
// - it returns data based on eveluations of 


/*
	 2.1 REGEX OPERATOR: $regex
	 -short for regular operations
	 -based on regular language
*/

/*
	2.1.1 Case sensitive query
	syntax:
		db.colectionName.find({field: {$regex: 'pattern'}})
*/
db.users.find({lastName: {$regex: 'A'}});

/*
	2.1.1 Case insensitive query
		utilized by using ($options: "i")
	syntax:
		db.colectionName.find({field: {$regex: 'pattern', $options: optionValue}})
*/
db.users.find({lastName: {$regex: 'A', $options: '$i'}});

db.users.find({firstName: {$regex: 'E', $options: '$i'}});


// 3. LOGICAL QUERY OPERATOR

/*
	 3.1 OR OPRATOR: "$or"
	 syntax:
		db.colectionName.find({field: {$or: [ {fieldA: valueA}, fieldB: ValueB]}})
*/
db.users.find({$or: [{firstName: "Neil" }, {age: "25"}]});
db.users.find({$or: [{firstName: "Neil" }, {age: {$gt: 30}}]});

/*
	 3.1 and OPRATOR: "$and"
	 syntax:
		db.colectionName.find({$or: [ {fieldA: valueA}, {fieldB: ValueB}]});
*/
db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});

db.users.find({$and: [{firstName: {$regex: 'E', $options: '$i'}}, {age: {$lte: 30}}]});

//FIELD PROJECTION
// in default mongoDB return the whole document. sometimes we dont need the whole document or we exclude some fields for recurity reason. 

/*
	1. ENCLUSION
		allow us to enclude specific field of the documents
		-we connot exclude on field using exclusion projection

		syntax
			db.collectionName.find({createria}, field:1})
*/

db.users.find({ firstName: "Jane" });

db.users.find(
{
	firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}

);


/*
	1.1 RETURNING SPECIFIC FIELDS IN EMBEDED DOCUMENTS
		- the double qoutation are important
*/
db.users.find(
{
	firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}

);

/*
	1.2 EXCEPTION TO THE INCLUSION RULE: supressing the ID field

		- allow us to exclude the _id field when retrieving documents
		syntax
			db.collectionName.find({createria}, _id:0})
*/
db.users.find(
{
	firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}

);

/*
	1.3 SLICE OPERATOR "$slice"
	- allows us to retrieve only one ellement that matches the createria
*/
db.users.insert({ 
		namearr: [
			{
				namea: "Juan"
			},
			{
				nameb: "Tamad"
			}
		]
});
db.users.find({
	namearr: 
	{
		namea: "Juan"
	}
});